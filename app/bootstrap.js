var app = angular.module('bootstrap', ['ui.bootstrap']);

app.config(function() {

}).run(['$http', function ($http) {
	$http({
		method: 'GET',
		url: '/api/setup'
	}).success(function (data, status, headers, config) {
		console.log(data);
		$script(data.files, function () {
			if (data.values) {
				angular.forEach(data.values, function (value, index) {
					angular.module(data.moduleName).value(index, value);
				});
			}
			angular.bootstrap(document.body, [data.moduleName]);
		});	
	});
}]);

angular.bootstrap(document.bootstrap, ['bootstrap']);