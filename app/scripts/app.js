var app = angular.module('app', ['ngRoute', 'ui.bootstrap']);

app.config(function($routeProvider, $locationProvider, $controllerProvider) {
	
	$routeProvider.when('/staff', {
		templateUrl: '/views/staff.html',
		controller: 'StaffCtrl'
	});
	
	$routeProvider.when('/admin', {
		templateUrl: '/views/admin',
		controller: 'AdminCtrl'
	});
});
