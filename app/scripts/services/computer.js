angular.module('app').factory('computerService', ['api', '$q', '$rootScope', function(api, $q, $rootScope) {
	
	$rootScope.computers = [];
	
	return {
		read : function () {
			var defer = $q.defer();
			
			api.get('/computers').then(function (result) {
				$rootScope.computers = result;
				defer.resolve(result);
			});
			
			return defer.promise;
		}
	}
}]);
	
