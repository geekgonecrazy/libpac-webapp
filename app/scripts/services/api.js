angular.module('app').factory('api', ['$rootScope', '$q', '$http', function($rootScope, $q, $http) {
	
	function request (method, url, data) {
		var defer = $q.defer();
		
	
        
        	$http({
			method: method,
			/*headers: {
				'Authorization': 'Bearer '+token
			},*/
			url: '/api'+url, 
			data: data
		}).success(function (data, status, headers, config) {

			if (typeof data.success !== 'undefined') {
				if (!data.success) {
					$log.error('[ERROR] URL: /api'+url+' Response: '+data.message);
				}
			}

			defer.resolve(data)
		});
		
		return defer.promise;
	}
	
	return {
		get : function (url) {
			var defer = $q.defer();
			
			request('get', url, {}).then(function (data) {
				defer.resolve(data);
			});
			
			return defer.promise;
		},
		post : function (url, data) {
			var defer = $q.defer();
			
			request('post', url, data).then(function (data) {
				defer.resolve(data);
			});
			
			return defer.promise;
		},
		put : function (url, data) {
			var defer = $q.defer();
			
			request('put', url, data).then(function (data) {
				defer.resolve(data);
			});
			
			return defer.promise;
		},
		delete : function (url, data) {
			var defer = $q.defer();
		
			request('delete', url, data || {}).then(function (data) {
				defer.resolve(data);
			});
		
			return defer.promise;
		}
	}
}]);
