angular.module('app').controller('UserCtrl', ['$scope', function($scope, $rootScope) {
	$scope.users = [
		{
			name: { 
				first: 'Aaron',
				last: 'Ogle'
			}, 
			status: 'online',
			timeRemaining: 60,
			lastComputer: 'Computer 1',
			notice: {
				type: 'warning'
			}
		},
	];
}]);