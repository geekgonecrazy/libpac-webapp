angular.module('app').controller('NotificationCtrl', ['$scope', '$log', 'notificationService', function($scope, $log, notificationService) {
	
	notificationService.subscribe();
	
	$scope.flipCard = function () {
		$scope.cardFlipped = ($scope.cardFlipped) ? false : true;
	}
	
	$scope.accept = function () {
		$scope.toast('testing 123');
		notificationService.subscribe();
	}
	
	$scope.reject = function ($index, notification) {
		$log.info($index, notification);
		$scope.notifications.splice($index, 1);
	}
}]);