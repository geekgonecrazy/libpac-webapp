angular.module('app').controller('wizardModal', ['$scope', '$q', 'apiConnect', '$modalInstance', 'steps', function ($scope, $q, apiConnect, $modalInstance, steps) {
	$scope.steps = steps;
	$scope.stepIndex = 0;
	$scope.maxSteps = $scope.steps.length-1;
	
	$scope.steps[$scope.stepIndex].active = true;
	$scope.activeStep = $scope.steps[$scope.stepIndex];
	
	$scope.nextStep = function () {
		if ($scope.stepIndex <= $scope.maxSteps) {
			
			$scope.postToAPI().then(function () {
				$scope.setStep($scope.stepIndex--);
			});
		}
	};
	
	$scope.finish = function () {
		$scope.postToAPI();	
	};
	
	$scope.previousStep = function () {
		if ($scope.stepIndex != 0) {
			$scope.setStep($scope.stepIndex--);
		}
	};
	
	$scope.setStep = function (index) {
		$scope.stepIndex = index;
		
		angular.forEach($scope.steps, function (step, i) {
			if (index === i) {
				step.active = true;	
			} else {
				step.active = false;	
			}
		});

		$scope.activeStep = $scope.steps[$scope.stepIndex];
	}
	
	$scope.postToAPI = function () {
		var defer = $q.defer();
		
		var data = {};
		
		angular.forEach($scope.activeStep.fields, function (field) {
			console.log(field.value, field.name);
			data[field.name] = field.value;
		});
		
		apiConnect.post($scope.activeStep.api, data).then(function (response) {
			console.log(response);
		});
		
		return defer.promise;
	};
	
	$scope.getPercentComplete = function () {
		var percent = ($scope.stepIndex+1) / ($scope.maxSteps+1) * 100;
		
		console.log('percent', percent);
		
		return {
			width: percent+'%'	
		}
	}
}]);