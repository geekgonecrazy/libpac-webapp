angular.module('app').controller('StaffCtrl', ['$scope', function($scope, $rootScope) {
	$scope.computerView = true;
	$scope.userView = false;
	
	$scope.showUsers = function() {
		$scope.computerView = false;
		$scope.userView = true;
	}
	
	$scope.showComputers = function() {
		$scope.computerView = true;
		$scope.userView = false;
	}
}]);