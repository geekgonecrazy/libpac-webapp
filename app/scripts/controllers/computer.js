angular.module('app').controller('ComputerCtrl', ['$scope', '$rootScope', '$modal', 'computerService', function($scope, $rootScope, $modal, computerService) {
	
	computerService.read();
	
	/*$scope.computers = [
		{name: 'Computer 1', status: 'online'},
		{name: 'Computer 2', status: 'offline'},
		{name: 'Computer 3', status: 'logged_in', guestName: 'Guest 1', timeRemaining: 58},
		{name: 'Computer 4', status: 'error'}
	];*/
		
	$scope.login = function(computer) {
		console.log(computer.name + ' Login');	
	}
		
	$scope.logout = function(computer) {
		console.log(computer.name + ' Logout');	
	}
		
	$scope.addTime = function(computer) {
		console.log(computer.name + ' Add Time');
		
		var modalInstance = $modal.open({
			templateUrl: '/views/modals/addTime.html',
			controller: 'addTimeCtrl',
			resolve: {
				computer : function () {
					return computer;	
				}
			}
		});
		
		computer.timeRemaining = computer.timeRemaining + 15;
	}
		
	$scope.subTime = function(computer) {
		console.log(computer.name + ' Sub Time');
		computer.timeRemaining = computer.timeRemaining - 15;
	}
		
	$scope.reserve = function() {
		console.log('Requesting computer be reserved');
	}
		
	$scope.announcement = function() {
		console.log('Making announcement');
	}
		
	$scope.shutdown = function(computer) {
		if (typeof computer !== 'undefined') {
			console.log(computer.name + ' Shutdown');
		} else {
			console.log('Shutdown All');
		}
	}
		
	$scope.restart = function(computer) {
		console.log(computer.name + ' Restart');	
	}
		
	$scope.restartServices = function(computer) {
		console.log(computer.name + ' Restart Services');
		console.log($scope.notifications);
		$scope.notifications.push({type: 'error', message: computer.name+' has experienced an error', action: 'restartServices'});
	}
		
	$scope.logIncident = function(computer) {
		console.log(computer.name + ' Log Incident');	
	}
		
	$scope.sendMessage = function(computer) {
		console.log(computer.name + ' Send Message');	
	}
}]);