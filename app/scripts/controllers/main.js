angular.module('app').controller('MainCtrl', ['$scope', 'socket', '$timeout', function($scope, socket, $timeout) {
	
	$scope.navigation = [
		{text: 'Home', url: '#'},
		{text: 'Staff', url: '#/staff'},
		{text: 'Admin', url: '#/admin'},
	];
		
	$scope.toast = function (message) {
		$scope.toastMessage = message;
		$scope.showToast = true;
		
		$timeout(function () {
			$scope.showToast = false;
		}, 3000);
	}
		
	socketio = socket;
}]);