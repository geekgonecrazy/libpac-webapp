angular.module('app').controller('setup', ['$scope', '$modal', 'wizardSteps', function ($scope, $modal, wizardSteps) {
	console.log('wizard steps', wizardSteps);
	
	var modalInstance = $modal.open({
		templateUrl: '/modules/setup/views/wizardModal.html',
		controller: 'wizardModal',
		
		size: 'lg',
		resolve: {
			steps: function () {
				return wizardSteps;
			}
		}
	});
}]);